package com.arisa.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank worawit = new BookBank("Worawit",50.0);   //Constructor
        worawit.print();

        BookBank aris = new BookBank("Aris",100000.0);
        aris.print();
        aris.withdraw(20000.0);
        aris.print();

        worawit.deposit(40000.0);
        worawit.print();
    }
}
